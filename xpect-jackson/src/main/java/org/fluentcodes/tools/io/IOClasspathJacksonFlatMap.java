package org.fluentcodes.tools.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Read files from the class path as with an object mapping for each file.
 */
public class IOClasspathJacksonFlatMap<T> {
    private final IOClasspathObjectList<Map<String, T>> ioList;

    public IOClasspathJacksonFlatMap(final String fileName, Class<?> modelClass) {
        this(fileName, StandardCharsets.UTF_8, modelClass);
    }

    public IOClasspathJacksonFlatMap(final String fileName, Charset encoding, Class<?> modelClass) {
        Class<?>[] listClass = new Class[]{Map.class, String.class, modelClass};
        ioList = new IOClasspathJacksonList<>(fileName, encoding, listClass);
    }

    public Map<String, T> read() {
        Map<String, T> flat = new HashMap<>();
        List<Map<String, T>> list = ioList.read();
        for (Map<String, T> cpEntry : list) {
            flat.putAll(cpEntry);
        }
        return flat;
    }
}

