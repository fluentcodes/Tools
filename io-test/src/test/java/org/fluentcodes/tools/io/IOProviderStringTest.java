package org.fluentcodes.tools.io;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;

import static org.fluentcodes.tools.io.TestObjectProvider.TEST_STRING;

public class IOProviderStringTest {
    public static final IOProviderString provider = new IOProviderString(Arrays.asList(
            new String[]{
                    "${test}=src/test/resources",
                    "ClasspathTest=test.txt",
                    "ReplaceTest=${test}/test.txt",
                    "ResourceDirect=src/test/resources/test.txt"
            }
    ));

    @Test
    public void testClassPath() {
        Assert.assertEquals(TEST_STRING, provider.getObject("ClasspathTest"));
    }

    @Test(expected = IORuntimeException.class)
    public void testNull() {
        provider.getObject(null);
    }

    @Test(expected = IORuntimeException.class)
    public void testNotExisting() {
        provider.getObject("blabla");
    }

    @Test
    public void testReplaceTest() {
        Assert.assertEquals(TEST_STRING, provider.getObject("ReplaceTest"));
    }

    @Test
    public void testResourceDirect() {
        Assert.assertEquals(TEST_STRING, provider.getObject("ResourceDirect"));
    }
}
