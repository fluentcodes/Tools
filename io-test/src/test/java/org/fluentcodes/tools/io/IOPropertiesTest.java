package org.fluentcodes.tools.io;

import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class IOPropertiesTest {
    @Test
    public void read() {
        IOProperties io = new IOProperties("application.properties");
        Properties properties = io.read();
        assertNotNull(properties);
        assertEquals("value", properties.get("key"));
    }

    @Test(expected = IORuntimeException.class)
    public void readFileNotExist() {
        new IOProperties("no.properties").read();
    }

    @Test
    public void readKeyNotExist() {
        Properties properties  = new IOProperties("application.properties").read();
        assertNull(properties.get("key2"));
    }
}
