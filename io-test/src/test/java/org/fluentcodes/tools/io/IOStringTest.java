package org.fluentcodes.tools.io;

import org.junit.Assert;
import org.junit.Test;

import static org.fluentcodes.tools.io.TestObjectProvider.TEST_FILE;
import static org.fluentcodes.tools.io.TestObjectProvider.TEST_STRING;

public class IOStringTest {

    @Test
    public void testWriteFile() {
        new IOString(TEST_FILE)
                .write(TEST_STRING);
        Assert.assertEquals(TEST_STRING, new IOString(TEST_FILE).read());
    }

    @Test
    public void testReadFile() {
        final String fromFile = new IOString(TEST_FILE)
                .read();
        Assert.assertNotNull(fromFile);
        Assert.assertEquals(TEST_STRING, fromFile);
    }

    @Test(expected = IORuntimeException.class)
    public void testReadFileNotExisting_throwsException() {
        new IOString("no.txt").read();
    }

    @Test
    public void testReadClassPath() {
        final String fromFile = new IOString("test.txt")
                .read();
        Assert.assertNotNull(fromFile);
        Assert.assertEquals(TEST_STRING, fromFile);
    }
}
