package org.fluentcodes.tools.io;

import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.fluentcodes.tools.io.TestObjectProvider.TEST_FILE;
import static org.fluentcodes.tools.io.TestObjectProvider.TEST_STRING;

public class IOBytesTest {

    @Test
    public void testWriteFile() {
        byte[] bytes = TEST_STRING.getBytes();
        new IOBytes(TEST_FILE)
                .write(bytes);
        byte[] bytesRead =new IOBytes(TEST_FILE).read();
        Assert.assertEquals(TEST_STRING, new String(bytesRead));
    }

    @Test
    public void testReadFile() {
        final String fromFile = new IOString(TEST_FILE)
                .read();
        Assert.assertNotNull(fromFile);
        Assert.assertEquals(TEST_STRING, fromFile);
    }

    @Test(expected = IORuntimeException.class)
    public void testReadFileNotExisting_throwsException() {
        new IOBytes("no.txt").read();
    }

    @Test
    public void testReadClassPath() {
        final byte[] fromFile = new IOBytes("test.txt")
                .read();
        Assert.assertNotNull(fromFile);
        Assert.assertEquals(TEST_STRING, new String(fromFile, StandardCharsets.UTF_8));
    }
}
