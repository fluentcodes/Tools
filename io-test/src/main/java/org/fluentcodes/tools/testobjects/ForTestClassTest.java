package org.fluentcodes.tools.testobjects;

import org.junit.Assert;
import org.junit.Test;

public class ForTestClassTest {

    @Test
    public void testOf1WithAssert() {
        ForTestClass of1 = ForTestClass.of1();
        Assert.assertNotEquals(of1, ForTestClass.of1());
    }
}
