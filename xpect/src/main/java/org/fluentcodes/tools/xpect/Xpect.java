package org.fluentcodes.tools.xpect;

import java.io.File;
import java.util.regex.Pattern;

/**
 * Interface for comparator.
 */
public interface Xpect {
    public Pattern CR_PATTERN = Pattern.compile("\\r");

    /**
     * Compare the object with the persisted defined in IO.
     *
     * @param toCompare the object to compare
     * @return the file used for persist
     */
    File assertEquals(final Object toCompare);


}
