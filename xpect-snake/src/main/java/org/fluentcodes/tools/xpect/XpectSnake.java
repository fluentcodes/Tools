package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.io.IOSnake;

public abstract class XpectSnake extends XpectAbstract{
    /**
     * Constructor with class array
     */
    public XpectSnake(Class annotationClass, Class<?>... classes) {
        super(new IOSnake(classes), "yaml", annotationClass);
    }
}
