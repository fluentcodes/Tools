package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.testobjects.Customer;
import org.fluentcodes.tools.testobjects.ForTestClass;
import org.junit.Test;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SnakeTest {

    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        map.put("key", "value");
        final DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        String value = new Yaml(options).dump(map);
        assertEquals("'1': test1\n" +
                "key: value\n", value);
    }

    @Test
    public void testForTestClass_ProblemFieldName() {
        ForTestClass object = ForTestClass.of1();
        final DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        StringWriter writer = new StringWriter();
        new Yaml(options).dump(object, writer);

        assertEquals("!!org.fluentcodes.tools.testobjects.ForTestClass {\n" +
                "  }\n", writer.toString());
    }

    @Test
    public void forTestClass_TestField() {
        ForTestClass object = new ForTestClass();
        object.setString("value");
        final DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        String result = new Yaml(options).dump(object);

        assertEquals("!!org.fluentcodes.tools.testobjects.ForTestClass {\n" +
                "  }\n", result);
    }

    @Test
    public void forTestClass_String_notWorking() {
        ForTestClass object = new ForTestClass();
        object.setString("value");
        final DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        String result = new Yaml(options).dump(object);

        assertEquals("!!org.fluentcodes.tools.testobjects.ForTestClass {\n" +
                "  }\n", result);
    }

    @Test
    public void testCustomer() {
        Customer customer = new Customer();
        customer.setAge(45);
        customer.setFirstName("Greg");
        customer.setLastName("McDowell");
        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(customer, writer);
        String expectedYaml = "!!org.fluentcodes.tools.testobjects.Customer {age: 45, contactDetails: null, firstName: Greg,\n" +
                "  homeAddress: null, lastName: McDowell}\n";

        assertEquals(expectedYaml, writer.toString());
    }

    @Test
    public void testCustomerPretty() {
        Customer customer = new Customer();
        customer.setAge(45);
        customer.setFirstName("Greg");
        customer.setLastName("McDowell");
        final DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        Yaml yaml = new Yaml(options);
        StringWriter writer = new StringWriter();
        yaml.dump(customer, writer);
        String expectedYaml = "!!org.fluentcodes.tools.testobjects.Customer\n" +
                "age: 45\n" +
                "contactDetails: null\n" +
                "firstName: Greg\n" +
                "homeAddress: null\n" +
                "lastName: McDowell\n";

        assertEquals(expectedYaml, writer.toString());
    }

}
