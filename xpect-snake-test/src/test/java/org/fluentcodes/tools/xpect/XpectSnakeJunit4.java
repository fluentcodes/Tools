package org.fluentcodes.tools.xpect;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Will compare the persisted file with an object as String via JUnit4.
 */
public class XpectSnakeJunit4 extends XpectSnake {
    public XpectSnakeJunit4(Class<?>... classes) {
        super(Test.class, classes);
    }

    public XpectSnakeJunit4(Object value) {
        super(Test.class, value.getClass());
    }

    public static File assertStatic(final Object toCompare) {
        XpectSnakeJunit4 xpect = new XpectSnakeJunit4(toCompare.getClass());
        return xpect.assertEquals(toCompare);
    }

    @Override
    public File assertEquals(final Object toCompare) {
        Assert.assertEquals(load(toCompare), getIo().asString(toCompare));
        return new File(getIo().getFileName());
    }


}
