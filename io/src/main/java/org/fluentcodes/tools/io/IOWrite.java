package org.fluentcodes.tools.io;

/**
 * Interface for IO classes.
 *
 * @param <T>
 */
public interface IOWrite<T extends Object> {
    /**
     * Write an object to the file system.
     *
     * @return The file found.
     */
    void write(T object);
}
