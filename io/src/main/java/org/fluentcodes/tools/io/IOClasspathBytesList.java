package org.fluentcodes.tools.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Read files from the classpath and returns a list of byte arrays.
 */
public class IOClasspathBytesList extends IOAbstract<List<byte[]>> {

    /**
     * The constructor without the file name.
     */
    public IOClasspathBytesList() {
        super();
    }

    /**
     * The constructor with the file name.
     *
     * @param fileName the file name
     */
    public IOClasspathBytesList(final String fileName) {
        super(fileName);
    }

    public String asString(List<byte[]> value) {
        return "";
    }

    public List<byte[]> asObject(String value) {
        return new ArrayList<>();
    }

    @Override
    public List<byte[]> read() {
        return read(false);
    }

    List<byte[]> read(boolean first) {
        if (!hasFileName()) {
            throw new IORuntimeException("No file name defined!");
        }
        try {
            Enumeration<URL> urlsFromClasspath = Thread.currentThread().getContextClassLoader().getResources(getFileName());
            return read(urlsFromClasspath, first);
        } catch (Exception e) {
            throw new IORuntimeException("Null matching file in classpath for '" + getFileName() + "'.");
        }
    }

    private List<byte[]> read(Enumeration<URL> urlsFromClasspath, boolean first) {
        List<byte[]> result = new ArrayList<>();
        while (urlsFromClasspath.hasMoreElements()) {
            URL url = urlsFromClasspath.nextElement();
            try {
                result.add(readFromInputStream(url.openStream()));
                if (first) {
                    return result;
                }
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }
        if (result.isEmpty()) {
            throw new IORuntimeException("Null matching file in classpath for '" + getFileName() + "'.");
        }
        return result;
    }

    byte[] readFromInputStream(InputStream stream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            byte[] chunk = new byte[4096];
            int bytesRead;
            while ((bytesRead = stream.read(chunk)) > 0) {
                outputStream.write(chunk, 0, bytesRead);
            }

        } catch (IOException e) {
            throw new IORuntimeException(e);
        }

        byte[] result = outputStream.toByteArray();
        try {
            outputStream.close();
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        return result;
    }
}

