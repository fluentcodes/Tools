package org.fluentcodes.tools.io;

import java.nio.charset.Charset;

public interface IOStringInterface {
    Charset getEncoding();

    default String asString(byte[] bytes) {
        return new String(bytes, getEncoding());
    }
}
