package org.fluentcodes.tools.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Read/Write byte from File.
 * read will try to read from classpath if not exists.
 */
public class IOBytes implements IO<byte[]>, IOWrite<byte[]> {
    private final IOClasspathBytesList ioBytesList;

    public IOBytes() {
        this.ioBytesList = new IOClasspathBytesList();
    }

    public IOBytes(final String fileName) {
        this.ioBytesList = new IOClasspathBytesList(fileName);
    }

    public String asString(byte[] value) {
        return new String(value);
    }

    public byte[] asObject(String value) {
        return value.getBytes();
    }

    @Override
    public void write(final byte[] bytes) {
        if (!hasFileName()) {
            throw new IORuntimeException("No file name defined!");
        }
        File file = new File(getFileName());
        try {
            write(new FileOutputStream(file), bytes);
        } catch (FileNotFoundException e) {
            throw new IORuntimeException(e);
        }
    }

    private void write(OutputStream stream, final byte[] bytes) {
        try {
            stream.write(bytes);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }
    }

    @Override
    public byte[] read() {
        if (!hasFileName()) {
            throw new IORuntimeException("No file name defined!");
        }
        File file = new File(getFileName());
        if (file.exists()) {
            try {
                return ioBytesList.readFromInputStream(new FileInputStream(file));
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }
        return ioBytesList.read(true).get(0);
    }

    @Override
    public String getFileName() {
        return ioBytesList.getFileName();
    }

    @Override
    public void setFileName(final String fileName) {
        ioBytesList.setFileName(fileName);
    }
}

