package org.fluentcodes.tools.io;

import java.io.File;

/**
 * Interface for IO classes.
 *
 * @param <T>
 */
public interface IO<T extends Object> {

    static void createDirs(File directory) {
        if (directory.exists()) {
            return;
        }
        File parentDirectory = directory.getParentFile();
        if (parentDirectory.exists()) {
            if (parentDirectory.isDirectory()) {
                directory.mkdir();
            } else {
                throw new IORuntimeException("Could not create directory " + directory.getAbsolutePath() + " since parent is not a directory.");
            }
        } else {
            createDirs(directory.getParentFile());
        }
    }

    /**
     * Read a file from the file system map the content to the object.
     *
     * @return the mapped object
     */
    T read();

    /**
     * Returns the local file name.
     *
     * @return the file name.
     */
    String getFileName();

    /**
     * Set the file name.
     *
     * @param fileName the file name
     */
    void setFileName(final String fileName);

    /**
     * Check file name set.
     *
     * @return
     */
    default boolean hasFileName() {
        return getFileName() != null && !getFileName().isEmpty();
    }

    /**
     * serialize the object.
     *
     * @param object the object to serialize
     * @return a string
     */
    String asString(T object);

    /**
     * Deserialize a String and returns the specified object.
     *
     * @param serialized the serialized object
     * @return the deserialized object
     */
    T asObject(String serialized);

}
