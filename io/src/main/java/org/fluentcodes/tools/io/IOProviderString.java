package org.fluentcodes.tools.io;

import java.util.Collection;
import java.util.Map;

public class IOProviderString extends IOProvider<String> {
    public IOProviderString(Collection<String> fileNameList) {
        super(fileNameList);
        Map<String, String> fileNames = super.getFileNames();
        for (Map.Entry<String, String> entry : fileNames.entrySet()) {
            super.put(entry.getKey(), new IOString(entry.getValue()));
        }
    }
}
