package org.fluentcodes.tools.io;

/**
 * Abstract class for storing mapping class array.
 *
 * @param <T> the mapped object.
 */
public abstract class IOMappingAbstract<T> implements IOMapping<T> {
    private final Class<?>[] mappingClasses;

    /**
     * The constructor with mapping classes. The array should have at least one value.
     *
     * @param mappingClasses mapping class array
     */
    IOMappingAbstract(Class<?>... mappingClasses) {
        this.mappingClasses = mappingClasses;
        if (mappingClasses.length == 0) {
            throw new IORuntimeException("Mapping classes must have at least 1 value");
        }
    }


    public Class<?>[] getMappingClasses() {
        return mappingClasses;
    }

}

