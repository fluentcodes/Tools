package org.fluentcodes.tools.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * A mapping IO object using gson.
 *
 * @param <T> the object class to map
 */
public class IOGson<T> extends IOMappingObject<T> {
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public IOGson(Class<?>... mappingClasses) {
        super(mappingClasses);
    }

    public IOGson(final String fileName, Class<?>... mappingClasses) {
        super(fileName, mappingClasses);
    }

    @Override
    public String asString(T object) {
        if (object == null) {
            throw new IORuntimeException("Null object for serialiazation!");
        }
        return GSON.toJson(object);
    }

    @Override
    public T asObject(final String asString) {
        return (T) GSON.fromJson(asString, getMappingClass());
    }
}
