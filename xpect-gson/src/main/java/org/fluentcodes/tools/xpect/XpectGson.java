package org.fluentcodes.tools.xpect;

import org.fluentcodes.tools.io.IOGson;


public abstract class XpectGson extends XpectAbstract{
    /**
     * Constructor with class array
     */
    public XpectGson(Class annotationClass, Class<?>... classes) {
        super(new IOGson<>(classes), "json", annotationClass);
    }
}
