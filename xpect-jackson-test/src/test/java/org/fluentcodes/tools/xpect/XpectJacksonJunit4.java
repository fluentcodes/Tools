package org.fluentcodes.tools.xpect;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Will compare the persisted file with an object as String via JUnit4.
 */
public class XpectJacksonJunit4 extends XpectJackson {
    public XpectJacksonJunit4(Class<?>... classes) {
         super(Test.class, classes);
    }

    public XpectJacksonJunit4(Object value) {
        super(Test.class, value.getClass());
    }

    public static File assertStatic(final Object toCompare) {
        XpectJacksonJunit4 xpect = new XpectJacksonJunit4(toCompare.getClass());
        return xpect.assertEquals(toCompare);
    }

    @Override
    public File assertEquals(final Object toCompare) {
        Assert.assertEquals(load(toCompare), getIo().asString(toCompare));
        return new File(getIo().getFileName());
    }
}
