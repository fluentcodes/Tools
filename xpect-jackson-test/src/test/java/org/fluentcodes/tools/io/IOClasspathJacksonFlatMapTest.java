package org.fluentcodes.tools.io;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.tools.xpect.XpectJacksonJunit4;
import org.junit.Test;

import java.util.Map;

public class IOClasspathJacksonFlatMapTest {

    @Test
    public void documentMapXpected() {
        IOClasspathJacksonFlatMap<DocumentMetadata> io =
                new IOClasspathJacksonFlatMap<>("documentMap.json", DocumentMetadata.class);
        Map<String, DocumentMetadata> documents = io.read();

        new XpectJacksonJunit4(String.class, DocumentMetadata.class).assertEquals(documents);
    }
}
