package org.fluentcodes.tools.io;

import org.fluentcodes.ihe.gematik.fdv.model.DocumentMetadata;
import org.fluentcodes.tools.xpect.XpectJacksonJunit4;
import org.junit.Test;
import java.util.List;
import java.util.Map;

public class IOClasspathJacksonTest {

    @Test
    public void documentXpected() {
        IOClasspathJacksonList<DocumentMetadata> io = new IOClasspathJacksonList<DocumentMetadata>(
                "document.json", DocumentMetadata.class);
        List<DocumentMetadata> documents = io.read();

        XpectJacksonJunit4.assertStatic(documents);
    }

    @Test
    public void documentMapXpected() {
        IOClasspathJacksonList<Map<String, DocumentMetadata>> io = new IOClasspathJacksonList<Map<String, DocumentMetadata>>(
                "documentMap.json", Map.class, String.class, DocumentMetadata.class);
        List<Map<String, DocumentMetadata>> documents = io.read();

        XpectJacksonJunit4.assertStatic(documents);
    }

    @Test
    public void documentListXpected() {
        IOClasspathJacksonList<List<DocumentMetadata>> io = new IOClasspathJacksonList<List<DocumentMetadata>>(
                "documentList.json", List.class, DocumentMetadata.class);
        List<List<DocumentMetadata>> documents = io.read();

        XpectJacksonJunit4.assertStatic(documents);
    }
}
