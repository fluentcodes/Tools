package org.fluentcodes.tools.xpect;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Will compare the persisted file with an object as String via JUnit4.
 */
public class XpectStringJunit4 extends XpectString implements Xpect {
    public XpectStringJunit4() {
        super(Test.class);
    }
    @Override
    public File assertEquals(final Object toCompare) {
        Assert.assertEquals(load(toCompare), getIo().asString(toCompare));
        return new File(getIo().getFileName());
    }
}
