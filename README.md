[fluentcodes.com](https://fluentcodes.com)

# Xpect
Xpect compare a whole test object with its E**Xpect**ation value persisted in a file in a human readable form.

By default this file is located in

* src/test/resources/Xpect/&lt;TEST_CLASS&gt;/&lt;TEST_METHOD&gt;.END

Independent of a test framework and the complexity of an object one can check complete results in one line with **assertEquals(&lt;object>)**.

The modules provided:
* io: Basic input classes
* xpect: Abstract classes for Xpect
* xpect-gson: Serialization with GSON (JSON experimental)
* xpect-jackson: Serialization with Jackson (JSON)
* xpect-jaxb: Serialization with Jaxb (XML)
* xpect-snake: Serialization with Snake (YAML experimental)

To avoid dependencies to a test framework the Xpect classes like e.g. **XpectJackson** are abstract.
The concrete implementation **XpectJacksonJunit4** with Junit4 you find in the test folder.

It implements the method assertEquals:

    @Override
    public File assertEquals(final Object toCompare) {
        Assert.assertEquals(load(toCompare), getIo().asString(toCompare));
        return new File(getIo().getFileName());
    }

In a test one can test an object in the following way:

    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        new ExpectJacksonJunit4(map.getClass()).assertEquals(map);
    }

A static wrapper simplify this call:

    @Test
    public void testHashMap() {
        Map map = new LinkedHashMap<>();
        map.put("1", "test1");
        ExpectJacksonJunit4.assertStatic(map);
    }

If the file does not exist it will be created. The content of the file src/test/resources/Xpect/XpectJacksonJunit4Test/testHashMap.json is

    {
        "1" : "test1"
    }

This is a rather simple example. But it works with any object regardless its complexity.

## Modules and Artifacts
### io
Some basic input/output classes which can be used independent of Xpect.

    <dependency>
        <groupId>org.fluentcodes.tools</groupId>
        <artifactId>io</artifactId>
        <version>0.9.1</version>
        <version>0.9.1</version>
    </dependency>

### xpect
The shared module with abstract classes and no benefit beside used by the different serialization modules.

Since the other modules depends on it, it's provided as a mvn repository:

    <dependency>
        <groupId>org.fluentcodes.tools</groupId>
        <artifactId>xpect</artifactId>
        <version>0.9.1</version>
        <version>0.9.1</version>
    </dependency>

### xpect-jackson
Serialization is done by [Jackson](https://github.com/FasterXML/jackson)
with ordered field names, so the results are comparable as a String. The example beforehand uses this module.

There is a mvn repository provided:

    <dependency>
        <groupId>org.fluentcodes.tools</groupId>
        <artifactId>xpect-jackson</artifactId>
        <version>0.9.1</version>
        <version>0.9.1</version>
    </dependency>

#### xpect-gson
Even Jackson is with spring a defacto standard it has some elementary restrictions serializing  
lists and maps without class definitions.
Gson works with untyped objects as expected.
This module contains a gson IO implementation and an XpectGson class.

    <dependency>
        <groupId>org.fluentcodes.tools</groupId>
        <artifactId>xpect-gson</artifactId>
        <version>0.9.1</version>
        <version>0.9.1</version>
    </dependency> 

### xpect-jaxb
xpect-jaxb uses JAXB for serialization:

    @Test
    public void testExtrinsicObject() {
        ExtrinsicObjectType document = new ExtrinsicObjectType();
        document.setId("ID");
        ExpectJaxbJunit4.assertEquals(document);
    }

The generated file is again rather simple, since only an attribute value is set. 

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <ExtrinsicObjectType id="ID" xmlns:ns="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0"/>

The class used in this example is [generated from EBRS schema files](https://github.com/fluentcodes/generated-ihe-ebRS) and used in the IHE meta data specification. 
It's one technology used for the german ehealth framework, a topic I've worked over the last year.

   <dependency>
      <groupId>org.fluentcodes.tools</groupId>
      <artifactId>xpect-jaxb</artifactId>
      <version>0.9.1</version>
      <version>0.9.1</version>
   </dependency>
        
#### xpect-snake
An experimental module I originally used for a analyzes of a schema yaml file.  
This module contains a snake yaml IO implementation and an XpectSnake class. 
Has some problems serializing the test objects with binary.

    <dependency>
        <groupId>org.fluentcodes.tools</groupId>
        <artifactId>xpect-snake</artifactId>
        <version>0.9.1</version>
        <version>0.9.1</version>
    </dependency> 

### Benefits
* An object could be revised in the serialized form in all its depth whereas an object with its hierarchical structure hides informations.
* The object is compared as a whole with one line.
* Differences are directly viewed in the IDE when executing the test.
* Simplify testing: No need to implement a plethora of assert statements.
* Simplify test object: the standardized way to store and read test objects helps to keep the resources folder clean. By its name its clear, to which test a file belongs!
