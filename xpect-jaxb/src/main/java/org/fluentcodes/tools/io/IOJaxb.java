package org.fluentcodes.tools.io;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;
import org.fluentcodes.tools.xpect.Xpect;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;


public class IOJaxb<T> extends IOMappingObject<T> {
    private QName qName;
    private String prefix = "ns";

    public IOJaxb(String fileName, Class<?> classes) {
        super(fileName, classes);
    }

    public IOJaxb(Class<?> classes) {
        super(classes);
    }

    @Override
    public T asObject(final String xml) {
        if (getMappingClass() == null) {
            throw new IORuntimeException("Only with class settings its possible to map xml");
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(getMappingClass());
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<T> element = (JAXBElement<T>) unmarshaller.unmarshal(new StreamSource(new StringReader(xml)), getMappingClass());
            return element.getValue();
        } catch (JAXBException e) {
            throw new IORuntimeException("Error while deserializing XML", e);
        }
    }

    @Override
    public String asString(T object) {
        if (object == null) {
            throw new IORuntimeException("Null object to serialize!");
        }
        JAXBElement<T> element = createElement(object);
        try {
            JAXBContext context = JAXBContext.newInstance(getMappingClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            //https://stackoverflow.com/questions/6895486/jaxb-need-namespace-prefix-to-all-the-elements
            marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", getNamespacePrefixMapper());
            StringWriter sw = new StringWriter();
            marshaller.marshal(element, sw);
            return Xpect.CR_PATTERN.matcher(sw.toString()).replaceAll("");
        } catch (Exception e) {
            throw new IORuntimeException(e);
        }
    }

    private NamespacePrefixMapper namespacePrefixMapper;

    public NamespacePrefixMapper getNamespacePrefixMapper() {
        if (namespacePrefixMapper == null) {
            namespacePrefixMapper = new NamespacePrefixMapper() {
                @Override
                public String getPreferredPrefix(String arg0, String arg1, boolean arg2) {
                    return prefix;
                }
            };
        }
        return namespacePrefixMapper;
    }

    public QName getQName() {
        return qName;
    }

    public IOJaxb<T> setQName(String qName) {
        this.qName = new QName("", qName);
        return this;
    }

    public IOJaxb<T> setQName(String nameSpace, String qName) {
        this.qName = new QName(nameSpace, qName);
        return this;
    }

    public boolean hasQName() {
        return qName != null;
    }

    public void setQName() {
        setQName(new QName("", getMappingClass().getSimpleName()));
    }

    public IOJaxb<T> setQName(QName qName) {
        this.qName = qName;
        return this;
    }

    // Used for those with no root element.
    public JAXBElement<T> createElement(T object) {
        if (object == null) {
            return null;
        }
        if (!hasQName()) {
            setQName();
        }
        return
                new JAXBElement(
                        qName,
                        getMappingClass(),
                        object);
    }


}
